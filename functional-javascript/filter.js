function getShortMessages(messages) {
  const newMessages = messages.filter(item => item.message.length < 50);
  return newMessages.map(obj => obj.message);
}

module.exports = getShortMessages;
