const checkUsersValid = goodUsers =>
submittedUsers => submittedUsers.every(user => goodUsers.some(obj => user.id === obj.id));

module.exports = checkUsersValid;
