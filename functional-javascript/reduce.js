function countWords(inputWords) {
  return inputWords.reduce((acc, current) => {

    if (!acc[current]) {
      acc[current] = 0;
    }

    acc[current] = acc[current] + 1;

    return acc;
  }, {});
}

module.exports = countWords;
