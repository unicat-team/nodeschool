import React from 'react'
import ReactDOM from 'react-dom'
    
export const FilterableProductTable = React.createClass({
  
  handleUserInput(filterText, inStockOnly) {
    this.setState({filterText,inStockOnly});
  },
  
  getInitialState: function() {
    return {filterText: '', inStockOnly: false};
  },
  
  render: function() {
    return (
      <div>
        <SearchBar filterText={this.state.filterText} inStockOnly={this.state.inStockOnly} onUserInput={this.handleUserInput}/>
        <ProductTable products={this.props.products}></ProductTable>
      </div>
    );
  }
});

export const SearchBar = React.createClass({
  
  handleChange() {
    const filterText = ReactDOM.findDOMNode(this.refs.filterText).value;
    const inStockOnly = ReactDOM.findDOMNode(this.refs.inStockOnly).checked;
    
    this.props.onUserInput(filterText, inStockOnly);
  },
  
  render: function() {
    return (
      <form>
        <input ref="filterText" type="search" placeholder="Search..." onChange={this.handleChange} value={this.props.filterText}/>
        <label>
          <input ref="inStockOnly" type="checkbox" onChange={this.handleChange} value={this.props.filterText}/>
          Only show products in stock
        </label>
      </form>
    );
  }
});

export const ProductTable = React.createClass({
  render () {
    const rows = this.props.products.reduce((acc,item, i, arr) => {
      if (i === 0 || arr[i-1].category !== item.category) {
        acc.push(<ProductCategoryRow>{item.category}</ProductCategoryRow>);
      }
      acc.push(<ProductRow name={item.name} price={item.price}></ProductRow>);
      return acc;
    },[])
    return (
      <table>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
});

export const ProductCategoryRow = React.createClass({
  render ()
  {
    return (<tr><th colSpan="2">{this.props.children}</th></tr>);
  }
});

export const ProductRow = React.createClass({
  render () {
    return (
     <tr>
        <td>{this.props.name}</td>
        <td>{this.props.price}</td>
      </tr>
    );
  }
});

